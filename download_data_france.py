import requests
import csv
import pandas as pd
import numpy as np
import os
import time


last_date_file = os.path.getmtime("covid-data-france-analysis/row_latest_france_data.csv")

if time.time() > (last_date_file + 86400):

    #The URL below is to download the french data regarding the positivity of tests in France, daily, per age group
    url = 'https://www.data.gouv.fr/en/datasets/r/dd0de5d9-b5a5-4503-930a-7b08dc0adc7c' 
    r = requests.get(url, allow_redirects=True)

    open('covid-data-france-analysis/row_latest_france_data.csv', 'wb').write(r.content)

    #Creation of the data frame and renaming appropriately 
    df = pd.read_csv("covid-data-france-analysis/row_latest_france_data.csv",  sep= ';')
    df = df.rename(columns={"fra":"France",
                            "jour":"Day",
                            "P_f":"N of + tests in W",
                            "P_h":"N of + tests in M",
                            "P":"N of + tests",
                            "T_f":"N of W tested",
                            "T_h":"N of M tested",
                            "T":"N of people tested",
                            "cl_age90":"Age class of people"})
    print(df.head())

    df.to_csv("covid-data-france-analysis/cleaned_latest_france_data.csv",sep=";")